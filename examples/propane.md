system {
    unit(kcal)
    unitcell(10.0, 10.0, 10.0, 90.0, 90.0, 90.0)
    config(propane.xyz)

    elements {
        C(12.011)
        H(1.0079)
    }

    molecules {
        Z {
            atoms {
                C(C, 4, -0.3, 3)
                H(H, 1,  0.1, 8)
            }

            bonds {
                harm(C-H, 676.9437, 1.093)
                harm(C-C, 682.9891, 1.543)
            }

            angles {
                harm(H-C-H, 87.2268, 109.47)
                harm(C-C-H, 77.8708, 109.47)
                harm(C-C-C, 64.7724, 111.70)
            }

            dihedrals {
                cos(H-C-C-H, 0.1149, 0.0000, 3.0000)
            }
        }

    }

    vdw {
        lj(Z.C, Z.H, 1.00, 0.11)
        lj(Z.C, Z.C, 2.00, 0.22)
    }
}
