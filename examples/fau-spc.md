system {
    unit(kcal)

    unitcell(24.2040067166, 24.2040067166, 24.2040067166, 90.0, 90.0, 90.0)

    config(fau-spc.xyz)

    elements {
        Si(28.0860)
         O(15.9994)
         C(12.0000)
         H( 1.0080)
    }

    molecules {

        FAU {
            atoms {
                Si(Si, 4,  2.0, 192)
                 O( O, 2, -1.0, 384)
            }

            bonds {
                harm(Si-O, 597.3200, 1.605)
            }
            angles {
                harm(O-Si-O, 138.1200, 109.50)
            }
            dihedrals {
                cos(Si-O-Si-O, 0.8060, 3, 0.00)
            }
        }

        SPC {
            atoms(1) {
                CA(C, 4, -0.3000,  2)
                CB(C, 4, -0.2000,  2)
                 H(H, 1,  0.1000, 10)
            }
            bonds {
                harm(CA-H , 676.9437, 1.093)
                harm(CA-CB, 682.9891, 1.543)
                harm(CB-CB, 682.9891, 1.543)
                harm(CB-H , 676.9437, 1.093)
            }
            angles {
                harm(H-CA-H  ,  71.3936, 109.47)
                harm(H-CA-CB ,  95.2874, 109.47)
                harm(H-CB-CB ,  95.2874, 109.47)
                harm(H-CB-CA ,  95.2874, 109.47)
                harm(CA-CB-CB, 164.6659, 109.50)
            }
            dihedrals {
                cos(CA-CB-CB-CA, 1.4225, 3, 0.0)
                cos(H-CA-CB-H  , 0.1149, 3, 0.0)
                cos(H-CB-CB-H  , 0.1149, 3, 0.0)
            }
        }

    }

    vdw {
        lj(FAU.Si, FAU.Si, 0.0496, 3.7410)
        lj(FAU.O , FAU.O , 0.1650, 2.9400)
        lj(FAU.Si, FAU.O , 0.0900, 3.3405)
        lj(SPC.H , FAU.O , 0.0567, 2.8510)
        lj(SPC.H , FAU.Si, 0.0948, 3.3000)
        lj(SPC.CA, FAU.O , 0.0812, 3.2030)
        lj(SPC.CB, FAU.O , 0.0812, 3.2030)
        lj(SPC.CA, FAU.Si, 0.0445, 3.6520)
        lj(SPC.CB, FAU.Si, 0.0445, 3.6520)
        lj(SPC.H , SPC.H , 0.0550, 2.6400)
        lj(SPC.CA, SPC.CA, 0.0514, 3.3440)
        lj(SPC.CB, SPC.CB, 0.0514, 3.3440)
        lj(SPC.CA, SPC.CB, 0.0514, 3.3440)
        lj(SPC.H , SPC.CA, 0.0532, 2.9920)
        lj(SPC.H , SPC.CB, 0.0532, 2.9920)
    }
}
