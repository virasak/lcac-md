name := "lcac-md"

version := "1.0.1"

scalaVersion := "2.8.2"

libraryDependencies += "commons-cli" % "commons-cli" % "1.2"

libraryDependencies += "org.scalatest" %% "scalatest" % "1.8" % "test"

seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)

