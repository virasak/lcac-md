package lcac.md

import org.scalatest.{WordSpec,matchers}
import matchers.ShouldMatchers
import scala.math.Pi
import lcac.math.zeroEps
import lcac.md.Vector.{X,Y,Z}


class UnitCellSpec extends WordSpec with ShouldMatchers {

  "An unit volume cubic unit cell" when {
    val unitLength = 1.0
    val halfPi = Pi/2
    val unitCubic = UnitCell(unitLength, unitLength, unitLength, halfPi, halfPi, halfPi)

    "create" should {

      "have all lattice size equal to %f that input" format (unitLength) in {
        unitCubic.a should be (unitLength)
        unitCubic.b should be (unitLength)
        unitCubic.c should be (unitLength)
      }

      "have all angle equal to PI/2" in {
        unitCubic.alpha should be (halfPi)
        unitCubic.beta  should be (halfPi)
        unitCubic.gamma should be (halfPi)
      }
    }

    "translate to lattice site (1, 0, 0)" should {
      val result = unitCubic.latticePoint(1, 0, 0)
      "produce the translation vector (1, 0, 0)" in {
        (result - X) forall zeroEps should be (true)
      }
    }
    "translate to lattice site (0, 1, 0)" should {
      val result = unitCubic.latticePoint(0, 1, 0)
      "produce the translation vector (0, 1, 0)" in {
        (result - Y) forall zeroEps should be (true)
      }
    }
    "translate to lattice site (0, 0, 1)" should {
      val result = unitCubic.latticePoint(0, 0, 1)
      "produce the translation vector (0, 0, 1)" in {
        (result - Z) forall zeroEps should be (true)
      }
    }
    "translate to lattice site (1, 1, 1)" should {
      val result = unitCubic.latticePoint(1, 1, 1)
      "produce the translation vector (1, 1, 1)" in {
        (result - Vector(1,1,1)) forall zeroEps should be (true)
      }
    }
  }
}
