package lcac

import scala.math.{abs,pow}

object math {

    /**
     * Machine epsilon for Double
     */
    val Eps = pow(2, -52)

    def equalsEps(a: Double, b: Double): Boolean = abs(a - b) < Eps

    def zeroEps(x: Double): Boolean = abs(x) < Eps
}
