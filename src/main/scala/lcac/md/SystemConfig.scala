package lcac.md

import field.nonbonding._
import field.bonding._

class SystemConfig(
  val molecules: List[Molecule],
  val unitCell: Option[UnitCell] = None,
  val twoBodyPotentials: List[TwoBodyPotential] = Nil,
  val threeBodyPotentials: List[ThreeBodyPotential] = Nil,
  val fourBodyPotentials: List[FourBodyPotential] = Nil,
  val energyUnit: String = "kcal") {

  def moleculeOf(atomType: AtomType): Option[Molecule] = {
    molecules.find(_.atomTypes.contains(atomType))
  }
}

