package lcac.md

import java.io.File
import scala.io.Source
import scala.util.parsing.combinator._

import io.AtomSource
import field.nonbonding._
import field.bonding._

class SystemConfigParsers(directory: File) extends RegexParsers {

  require(directory.isDirectory)

  import SystemConfigParsers._


  var elementTypes: List[ElementType] = Nil

  var atomTypes: List[AtomType] = Nil

  var moleculeLists: List[Molecule] = Nil

  var numberOfSkipAtoms = 0

  var unitCellVar: Option[UnitCell] = None

  var inputFile: java.io.File = _

  def ident = """(\w)+""".r ^^ {x => x}

  def filePath = System.getProperty("file.separator") match {
      case """/""" => """[a-zA-Z0-9./_-]+""".r ^^ { x => x.trim }
      case """\""" => """[a-zA-Z0-9.\\_-]+""".r ^^ { x => x.trim }
      case _ =>  throw new RuntimeException("Unknown System path separator")
  }

  def floatingValue = """(-)?\d+(\.\d+)?(E(-)?\d+)?""".r ^^ { x => x.toDouble }

  def intValue = """(-)?\d+""".r ^^ { x => x.toInt }

  def params[A](value: Parser[A]): Parser[A] = "(" ~> value <~ ")"

  def body[A](value: Parser[A]): Parser[A] = "{" ~> value <~ "}"

  def elementType: Parser[ElementType] = ident ^^ {
    case name => elementTypes.find(_.name == name).get
  }

  def atomType: Parser[AtomType] = ident ^^ {
    case name => atomTypes.find(_.name == name).get
  }

  def system: Parser[SystemConfig] = "system"~>body(systemBody)

  def systemBody: Parser[SystemConfig] =
    energyUnit~unitCell~config~elements~molecules~twobody~threebody~fourbody ^^ {
      case unit~uc~cf~elems~mols~two~three~four =>
        new SystemConfig(mols, unitCellVar, two, three, four, unit)
    }

  def energyUnit: Parser[String] = opt("unit"~>params(ident))^^ {
    case Some(unit) => unit
    case None => defaultEnergyUnit
  }

  def unitCell: Parser[Option[UnitCell]] = opt("unitcell"~>params(repsep(floatingValue, ",")))^^{
    // non periodic
    case None => None

    // user defined unitcell
    case Some(list) if list.size == 6 =>
      val a = list(0)
      val b = list(1)
      val c = list(2)
      val alpha = math.toRadians(list(3))
      val beta = math.toRadians(list(4))
      val gamma = math.toRadians(list(5))
      unitCellVar = Some( UnitCell(a, b, c, alpha, beta, gamma))

      unitCellVar

    // shorter or longer parameters
    case Some(_) =>
      throw new RuntimeException("number of unitcell parameter must equal to 6")
  }

  def config: Parser[java.io.File] = ("config"~>params(filePath)) ^^ {
      case fileName =>
        inputFile = new java.io.File(directory, fileName)
        inputFile
  }

  def elements: Parser[List[ElementType]] = "elements" ~> body(elementsBody)

  def elementsBody: Parser[List[ElementType]] = rep(element) ^^ {
    case list =>
      elementTypes = list
      list
  }

  def element: Parser[ElementType] = ident~params(floatingValue) ^^ {
    case name~atomicMass => ElementType(name, atomicMass)
  }

  def unitCellBody: Parser[List[Double]] = rep(floatingValue)

  def molecules: Parser[List[Molecule]] = "molecules"~>body(rep1(molecule)) ^^ {
    case mols =>
      moleculeLists = mols
      moleculeLists
  }

  def molecule: Parser[Molecule] = ident~body(moleculeBody) ^^ {
    case name~((atms, bnds, angs, dihs)) =>
      val (virtualBondTypes, nbonds) = bnds.partition(x => x.isInstanceOf[VirtualBondType])
      val vbonds = virtualBondTypes.map(_.asInstanceOf[VirtualBondType])
      val mol = new Molecule(name, atms, nbonds, vbonds, angs, dihs, unitCellVar)

      numberOfSkipAtoms += mol.numberOfAtoms

      mol
  }

  def moleculeBody: Parser[(List[AtomType],List[BondType],List[AngleType],List[DihedralType])] =
    atoms ~ opt(bonds ~ opt(angles ~ opt(dihedrals))) ^^ {
      case ats~None                          => (ats, Nil, Nil, Nil)
      case ats~Some(bnd~None)                => (ats, bnd, Nil, Nil)
      case ats~Some(bnd~Some(ang~None))      => (ats, bnd, ang, Nil)
      case ats~Some(bnd~Some(ang~Some(dih))) => (ats, bnd, ang, dih)
    }


  // atoms

  def atoms: Parser[List[AtomType]] =
    ("atoms"~>atomParams)~body(rep(pdbAtomBody|xyzAtomBody)) ^^ {
      case n~atms => atms.head match {
          case _: Pair[_,_] =>
            val src = Source.fromFile(inputFile)
            atomTypes = AtomSource.fromXYZ(src, numberOfSkipAtoms, atms.asInstanceOf[List[(AtomType,Int)]], n)
            atomTypes
          case _: AtomType =>
            val src = Source.fromFile(inputFile)
            atomTypes = AtomSource.fromPDB(src, atms.asInstanceOf[List[AtomType]])
            atomTypes
      }
  }

  def atomParams: Parser[Int] = opt(params(intValue)) ^^ {
    case Some(n) => n
    case None => defaultRepeat
  }

  def xyzAtomBody: Parser[(AtomType,Int)] =
    ident~params(elementType~(","~>intValue)~(","~>floatingValue)~(","~>intValue)) ^^ {

    case name~(et~nb~ch~nrept) => (AtomType(name, et, nb, ch), nrept)
  }

  def pdbAtomBody: Parser[AtomType] =
    ident~params(elementType~(","~>intValue)~(","~>floatingValue)) ^^ {

    case name~(et~nb~ch) => AtomType(name, et, nb, ch)
  }
  // bonds

  def bonds: Parser[List[BondType]] = "bonds" ~> body(rep(bond)) ^^ {
    case xs =>
      val foundDup = for ((x, i) <- xs.zipWithIndex; y <- xs.drop(i + 1) if x equiv y) yield x
      if (foundDup.size > 0) {
        throw new RuntimeException("found some duplicated bond types: " + foundDup.mkString(", "))
      } else {
        xs
      }
  }

  def bond: Parser[BondType] = ident~params((atomType<~"-")~atomType~opt(",")~(repsep(floatingValue, ","))) ^^ {
      case _~(_~_~Some(",")~Nil) => throw new RuntimeException("found ',' in wrong position")
      case btype~(atype1~atype2~_~pars) => createBondType(btype, atype1, atype2, pars)
  }

  // angles 

  def angles: Parser[List[AngleType]] = "angles"~>body(anglesBody) ^^ {
    case xs =>
      val foundDup = for ((x, i) <- xs.zipWithIndex; y <- xs.drop(i + 1) if x equiv y) yield x
      if (foundDup.size > 0) {
        throw new RuntimeException("found some duplicated bond angles: " + foundDup.mkString(", "))
      } else {
        xs
      }
  }

  def anglesBody: Parser[List[AngleType]] = rep(angle)

  def angle: Parser[AngleType] = ident~params((atomType<~"-")~(atomType<~"-")~(atomType<~",")~repsep(floatingValue,",")) ^^ {
    case angtype~(atype1~atype2~atype3~pars) =>
      createAngleType(angtype, atype1, atype2, atype3, pars)
  }


  // dihedrals

  def dihedrals: Parser[List[DihedralType]] = "dihedrals"~>body(dihedralsBody) ^^ {
    case xs =>
      val foundDup = for ((x, i) <- xs.zipWithIndex; y <- xs.drop(i + 1) if x equiv y) yield x
      if (foundDup.size > 0) {
        throw new RuntimeException("found some duplicated bond dihedrals: " + foundDup.mkString(", "))
      } else {
        xs
      }
  }

  def dihedralsBody: Parser[List[DihedralType]] = rep(dihedral)

  def dihedral: Parser[DihedralType] = ident~params((atomType<~"-")~(atomType<~"-")~(atomType<~"-")~(atomType<~",")~repsep(floatingValue, ",")) ^^ {
    case dtype~(atype1~atype2~atype3~atype4~pars) =>
      createDihedralType(dtype, atype1, atype2, atype3, atype4, pars)
  }

  def twobody: Parser[List[TwoBodyPotential]] = opt("vdw"~>body(twobodyBody))^^{
    case None => Nil
    case Some(xs) => xs
  }

  def atomTypeInMolecule: Parser[AtomType] = (ident<~".")~ident ^^ {
    case mname~aname =>
      val molecule = moleculeLists.find(_.name == mname).getOrElse {
        throw new RuntimeException("Not found molecule '%s'".format(mname))
      }
      molecule.atomTypes.find(_.name == aname).getOrElse {
        throw new RuntimeException("Not found atom type '%s' in molecule '%s'".format(mname, aname))
      }
  }

  def twobodyBody: Parser[List[TwoBodyPotential]] = rep(twobodyDef)

  def twobodyDef: Parser[TwoBodyPotential] = ident~params((atomTypeInMolecule<~",")~(atomTypeInMolecule<~",")~(repsep(floatingValue,","))) ^^ {
    case name~(a~b~pars) => createTwoBodyPotential(name, a, b, pars)
  }

  def threebody: Parser[List[ThreeBodyPotential]] = opt("tbp"~>body(threebodyBody))^^{
    case None => Nil
    case Some(xs) => xs
  }

  def threebodyBody: Parser[List[ThreeBodyPotential]] = rep(threebodyDef)

  def threebodyDef: Parser[ThreeBodyPotential] = ident~params((atomTypeInMolecule<~",")~(atomTypeInMolecule<~",")~(atomTypeInMolecule<~",")~(repsep(floatingValue,","))) ^^ {
    case name~(a~b~c~pars) => createThreeBodyPotential(name, a, b, c, pars)
  }

  def fourbody: Parser[List[FourBodyPotential]] = opt("fbp"~>body(fourbodyBody))^^{
    case None => Nil
    case Some(xs) => xs
  }

  def fourbodyBody: Parser[List[FourBodyPotential]] = rep(fourbodyDef)

  def fourbodyDef: Parser[FourBodyPotential] = ident~params((atomTypeInMolecule<~",")~(atomTypeInMolecule<~",")~(atomTypeInMolecule<~",")~(atomTypeInMolecule<~",")~(repsep(floatingValue,","))) ^^ {
    case name~(a~b~c~d~pars) => createFourBodyPotential(name, a, b, c, d, pars)
  }
}


object SystemConfigParsers {


  import math._

  val defaultEnergyUnit = "kcal"

  val defaultRepeat = 1

  def createBondType(name: String, a: AtomType, b: AtomType, params: List[Double]): BondType = name match {
    case "virtual" =>
        params.size match {
          case 0 => VirtualBondType(a, b)
          case 1 => VirtualBondType(a, b, params(0).toInt)
          case _ => throw new RuntimeException("virtual bond require zero or one parameter.")
        }
    case "a12r6" =>
      require(params.size == 2)
      A12R6BondType(a, b, params(0), params(1))
    case "harm" =>
      require(params.size == 2)
      HarmonicBondType(a, b, params(0), params(1))
    case "mors" =>
      require(params.size == 3)
      MorseBondType(a, b, params(0), params(1), params(2))
  }

  def createAngleType(name: String, a: AtomType, b: AtomType, c: AtomType, params: List[Double]): AngleType = name match {

    case "hcos" =>
      require(params.size == 2)
      HCosAngleType(a, b, c, params(0), params(1))
    case "harm" =>
      require(params.size == 2)
      HarmonicAngleType(a, b, c, params(0), params(1))
    case "quar" =>
      require(params.size == 4)
      QuarticAngleType(a, b, c, params(0), params(1), params(2), params(3))
    case _ => throw new RuntimeException("unknown angle type %s".format(name))
  }

  def createDihedralType(name: String, a: AtomType, b: AtomType, c: AtomType, d: AtomType, params: List[Double]): DihedralType = {
    name match {
      case "harm" =>
        require(params.size == 2)
        HarmonicType(a, b, c, d, params(0), params(1))
      case "cos" =>
        require(params.size == 3)
        CosDihedralType(a, b, c, d, params(0), params(1), params(2))
      case _ => throw new RuntimeException("Unknown dihedral type %s".format(name))
    }
  }

  def createFourBodyPotential(name: String, a: AtomType, b: AtomType, c: AtomType, d: AtomType, params: List[Double]): FourBodyPotential = {
    name match {
      case "harm" =>
        require(params.size == 2)
        Harmonic4(a, b, c, d, params(0), params(1))
      case _ => throw new RuntimeException("Unknown three body potential was found: %s".format(name))
    }
  }

  def createThreeBodyPotential(name: String, a: AtomType, b: AtomType, c: AtomType, params: List[Double]): ThreeBodyPotential = {
    name match {
      case "thrm" =>
        require(params.size == 5)
        TruncatedHarmonic3(a, b, c, params(0), params(1), params(2), params(3), params(4))
      case _ =>
        throw new RuntimeException("Unknown three body potential was found: %s".format(name))
    }
  }

  def createTwoBodyPotential(name: String, a: AtomType, b: AtomType, params: List[Double]): TwoBodyPotential = {
    name match {
        case "lj" =>
          require(params.size == 2)
          LernnardJones(a, b, params(0), params(1))
        case _ =>
          throw new RuntimeException("Unknown two body potential was found: %s".format(name))
    }
  }
}
