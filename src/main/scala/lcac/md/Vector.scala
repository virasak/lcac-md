package lcac.md

case class Vector(x: Double, y: Double, z: Double) {

  def distance(that: Vector): Double = math.sqrt(distance2(that))

  def distance2(that: Vector): Double = {
    val dx = (x - that.x)
    val dy = (y - that.y)
    val dz = (z - that.z)

    (dx * dx + dy * dy + dz * dz)
  }

  def -(that: Vector): Vector = Vector(x - that.x, y - that.y, z - that.z)

  def dot(that: Vector): Double = x * that.x + y * that.y + z * that.z

  def cross(that: Vector): Vector = Vector(y*that.z - z * that.y, z*that.x - x*that.z, x*that.y - y*that.x)

  lazy val length: Double = math.sqrt(this dot this)

  def /(v: Double): Vector = Vector(x/v, y/v, z/v)

  def unit: Vector = this / length

  def angle(that: Vector): Double = {
    val cosTheta = this.unit dot that.unit
    if (math.abs(math.abs(cosTheta) - 1) < 1E-15) 0 else math.acos(cosTheta)
  }
}

object Vector {

  val origin = new Vector(0, 0, 0)
  val X = Vector(1, 0, 0)
  val Y = Vector(0, 1, 0)
  val Z = Vector(0, 0, 1)


  implicit def toList(v: Vector): List[Double] = List(v.x, v.y, v.z)

}
