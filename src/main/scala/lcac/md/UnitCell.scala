package lcac.md


object UnitCell {
  def fromVectors(a: Vector, b: Vector, c: Vector): UnitCell = {
    UnitCell(a.length, b.length, c.length, b angle c, c angle a, a angle b)
  }
}

/**
 *
 */
case class UnitCell(a: Double, b: Double, c: Double, alpha: Double, beta: Double, gamma: Double) {

  import math.{cos,sin,sqrt,toRadians}

  /**
   * All translation vectors are got from
   *     Edward Prince, "Mathematical Techniques in Crystallography and Materials Science",
   *                    Springer-Verlag, 1994, p.18.
   */
  val xTrans = Vector(a, b*cos(gamma), c*cos(beta))
  val yTrans = Vector(0, b*sin(gamma), c*(cos(alpha) - cos(beta)*cos(gamma))/sin(gamma))
  val zTrans = Vector(0, 0, c*sqrt(1 - cos(alpha)*cos(alpha) - cos(beta)*cos(beta) - cos(gamma)*cos(gamma) + 2*cos(alpha)*cos(beta)*cos(gamma))/sin(gamma))

  lazy val minWidth: Double = {
    List(a, b, c).reduceLeft(math.min) // FIXME
  }

  def imagesOf(position: Vector, shells: Int): Iterable[Vector] = {
    require(shells > 0)

    val indices = (-shells) to shells

    for {
      l <- indices; m <- indices; n <- indices
      if (l != 0 || m != 0 || n != 0)
      lmn = Vector(l, m, n)
      dx = xTrans dot lmn
      dy = yTrans dot lmn
      dz = zTrans dot lmn
    } yield Vector(position.x + dx, position.y + dy, position.z + dz)
  }

  def latticePoint(l: Int, m: Int, n: Int): Vector = {
    val lmn = Vector(l, m, n)
    Vector(xTrans dot lmn, yTrans dot lmn, zTrans dot lmn)
  }


  val latticeSystem: LatticeSystem = {
    val angles = List(alpha, beta, gamma) map (_ == toRadians(90.0))
    val equals90 = { (x: Boolean) => x == true }
    val edges = List(a, b, c).distinct.size

    if (angles exists equals90) {
      if (angles forall equals90) {
        edges match {
          case 1 => Cubic // a == b == c, alpha == beta == gamma == 90
          case 2 => Tetragonal // a == b != c, alpha == beta == gamma == 90
          case 3 => Orthorhombic // a != b != c, alpha == beta == gamma == 90
        }
      } else {
        angles count equals90 match {
          case 2 if gamma == toRadians(120.0) && a == b => Hexagonal // a == b != c, alpha == beta == 90, gamma == 120
          case 2 => Monoclinic
          case _ => throw new RuntimeException("Unknown lattice system")
        }
      }

    } else if (alpha == beta && beta == gamma) {
      Rhombohedral // alpha == beta == gamma != 90
    } else {
      Triclinic // alpha, beta, gamma != 90
    }
  }
}

sealed trait LatticeSystem
object Triclinic extends LatticeSystem
object Monoclinic extends LatticeSystem
object Orthorhombic extends LatticeSystem
object Rhombohedral extends LatticeSystem
object Tetragonal extends LatticeSystem
object Hexagonal extends LatticeSystem
object Cubic extends LatticeSystem

