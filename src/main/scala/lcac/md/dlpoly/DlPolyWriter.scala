package lcac.md.dlpoly

import scala.io.Source
import java.io.PrintStream

import lcac.md._
import lcac.md.field.nonbonding._
import lcac.md.field.bonding._

/**
 *
 */
class DlPolyWriter(val system: SystemConfig, debug: Boolean) {

  def imcon: Int = if (system.unitCell == None) {
      0 // nonperiodic
    } else {
      system.unitCell.get.latticeSystem match {
        case Cubic => 1
        case Orthorhombic => 2
        case _ => 3
      }
  }


  /**
   *
   */
  def writeConfig(out: PrintStream) {
    println("start write config file")
      // CONFIG File
    val levcfg = 0 // TODO and velocity and force options
    out.println("DlPoly generated file")
    out.println("%10d%10d".format(levcfg, imcon))

    if (system.unitCell != None) {
        val uc = system.unitCell.get
        val a = uc.latticePoint(1, 0, 0)
        val b = uc.latticePoint(0, 1, 0)
        val c = uc.latticePoint(0, 0, 1)

        out.println("%20.6f%20.6f%20.6f".format(a.x, a.y, a.z))
        out.println("%20.6f%20.6f%20.6f".format(b.x, b.y, b.z))
        out.println("%20.6f%20.6f%20.6f".format(c.x, c.y, c.z))
    }

    for {
      molecule <- system.molecules
      (atom, index) <- molecule.config.zipWithIndex
    } {
      out.println("%8s%10d".format(molecule.name + atom.atomType.name, index + 1))
      out.println("%20.6f%20.6f%20.6f".format(atom.x, atom.y, atom.z))
    }

    println("finish write config file")
  }

  /**
   *
   */
  def writeField(out: PrintStream) {
    println("start write field file")

    out.println("DlPolyHelper generated file")
    out.println("UNITS %s".format(system.energyUnit))
    out.println
    out.println("MOLECULES %d".format(system.molecules.size))

    system.molecules.foreach {
      molecule => writeMoleculeField(out, molecule)
    }

    writeTwoBodyPotentials(out, system.twoBodyPotentials)
    writeThreeBodyPotentials(out, system.threeBodyPotentials)
    writeFourBodyPotentials(out, system.fourBodyPotentials)

    out.println("CLOSE")
  }

  /**
   *
   */
  def writeTwoBodyPotentials(out: PrintStream, twoBodyPotentials: List[TwoBodyPotential]) {
    if (twoBodyPotentials == Nil) () else {
      out.println("VDW %d".format(twoBodyPotentials.size))
      twoBodyPotentials.foreach { tbp =>
        val aName = system.moleculeOf(tbp.aType).get.name + tbp.aType.name
        val bName = system.moleculeOf(tbp.bType).get.name + tbp.bType.name
        out.print("%8s %8s".format(aName, bName))
        tbp match {
          case lj: LernnardJones =>
            out.println(" %8s %12.6f %12.6f".format("lj", lj.epsilon, lj.zigma))
          case _ => out.println("Unknown")
        }
      }
    }
  }

  /**
   *
   */
  def writeThreeBodyPotentials(out: PrintStream, threeBodyPotentials: List[ThreeBodyPotential]) {
    if (threeBodyPotentials == Nil) () else {
      out.println("TBP %d".format(threeBodyPotentials.size))
      threeBodyPotentials.foreach { tbp =>
        val aName = system.moleculeOf(tbp.aType).get.name + tbp.aType.name
        val bName = system.moleculeOf(tbp.bType).get.name + tbp.bType.name
        val cName = system.moleculeOf(tbp.cType).get.name + tbp.cType.name
        out.print("%8s %8s %8s".format(aName, bName, cName))
        tbp match {
          case thrm: TruncatedHarmonic3 =>
                     out.println(" %8s %12.6f %12.6f %12.6f".format("thrm", thrm.k, thrm.zeta, thrm.rho))
                       case _ => out.println("unknown")
        }
      }
    }
  }

  /**
   *
   */
  def writeFourBodyPotentials(out: PrintStream, fourBodyPotentials: List[FourBodyPotential]) {
    if (fourBodyPotentials == Nil) () else {
      out.println("FBP %d".format(fourBodyPotentials.size))
      fourBodyPotentials.foreach { fbp =>
        val aName = system.moleculeOf(fbp.aType).get.name + fbp.aType.name
        val bName = system.moleculeOf(fbp.bType).get.name + fbp.bType.name
        val cName = system.moleculeOf(fbp.cType).get.name + fbp.cType.name
        val dName = system.moleculeOf(fbp.dType).get.name + fbp.dType.name
        out.print("%8s %8s %8s %8s".format(aName, bName, cName, dName))
        fbp match {
          case hrm: Harmonic4 =>
            out.println(" %8s %12.6f %12.6f".format("harm", hrm.k, hrm.psi))
          case _ => out.println("unknown")
        }
      }
    }
  }

  /**
   *
   */
  def writeMoleculeField(out: PrintStream, molecule: Molecule) {
    println("start write molecule %s".format(molecule.name))

    out.println(molecule.name)
    out.println("NUMMOLS 1")

    // FIELD File: Atom
    printAtomTypes(out, molecule)

    if (molecule.bonds == Nil) () else {
      println("start write bonds")
      printBonds(out, molecule)
      println("finish write bonds")

      // FIELD File: Angle
      println("start write angles")
      printAngles(out, molecule)
      println("finish write angles")

      // FILED File: Dihedral
      println("start write dihedrals")
      printDihedrals(out, molecule)
      println("finish write dihedrals")
    }

    out.println("FINISH")
  }

  /**
   *
   */
  def printAtomTypes(out: PrintStream, molecule: Molecule) = {
    out.println("ATOMS %d".format(molecule.numberOfAtoms))
    molecule.atomTypes.foreach { atomType =>
      val msg = "%8s %12.6f %12.6f %8d"
      out.println(msg.format(
        molecule.name + atomType.name,
        atomType.elementType.atomicMass,
        atomType.charge,
        atomType.numberOfAtoms))
    }
  }

  /**
   *
   */
  def printBonds(out: PrintStream, molecule: Molecule) = if (molecule.bonds != Nil)  {
    val bonds = molecule.bonds
    val config = molecule.config
    out.println("BONDS %d".format(bonds.size))
    bonds.foreach { bond =>
      val (bondTypeName, params) = bond.bondType match {
        case a: A12R6BondType => ("12-6", List(a.a, a.b))
        case h: HarmonicBondType => ("harm", List(h.k, h.r0))
        case m: MorseBondType => ("mors", List(m.e0, m.r0, m.k))
        case _ => ("unknown", Nil)
      }

      if (debug) {
        out.print("%s %12.6f ".format( bond.bondType, bond.distance))
      }

      out.print("%-4s %8d %8d".format(bondTypeName, config.indexWhere(_.real == bond.atom1.real) + 1, config.indexWhere(_.real == bond.atom2.real) + 1))

      params.foreach { param => out.print(" %12.6f".format(param)) }

      out.println
    }

    if (debug) {
      bonds.groupBy(_.bondType).foreach {
        case (bondType,bs) => println("%s #%d" format (bondType, bs.size))
      }
    }
  }

  /**
   *
   */
  def printAngles(out: PrintStream, molecule: Molecule) = {
    val angles = molecule.angles
    if (angles.hasNext) {
      val config = molecule.config
      val angleTempFile = java.io.File.createTempFile("angle", "tmp")
      angleTempFile.deleteOnExit
      val ats = new PrintStream(angleTempFile)
      var acount = 0
      val angleTypeCount = scala.collection.mutable.Map[AngleType,Int]()
      angles.foreach { angle =>
        acount = acount + 1
        angleTypeCount(angle.angleType) = 1 + angleTypeCount.getOrElse(angle.angleType, 0)
        val (angleTypeName, params) = angle.angleType match {
          case harm: HCosAngleType => ("hcos", List(harm.k, harm.zeta))
          case harm: HarmonicAngleType => ("harm", List(harm.k, harm.zeta))
          case quar: QuarticAngleType => ("quar", List(quar.k, quar.zeta, quar.kp, quar.kpp))
          case _ => ("unknown", Nil)
        }

        if (debug) {
          ats.print("%s %-5.2f ".format( angle.angleType, math.toDegrees(angle.angle)))
        }

        ats.print("%-4s %8d %8d %8d".format(angleTypeName, config.indexWhere(_.real == angle.atom1.real) + 1, config.indexWhere(_.real == angle.atom2.real) + 1, config.indexWhere(_.real == angle.atom3.real) + 1))
        params.foreach { param => ats.print(" %12.6f".format(param)) }
        ats.println
      }
      ats.close

      if (debug) {
        angleTypeCount.foreach {
          case (angleType, count) => println("%s #%d" format (angleType, count))
        }
      }

      out.println("ANGLES %d".format(acount))
      Source.fromFile(angleTempFile).getLines().foreach {
        line => out.println(line)
      }
    }
  }

  /**
   *
   */
  def printDihedrals(out: PrintStream, molecule: Molecule) = {
    val dihedrals = molecule.dihedrals
    if (dihedrals.hasNext) {
      val config = molecule.config
      val dihedralTempFile = java.io.File.createTempFile("dihedral", "tmp")
      dihedralTempFile.deleteOnExit
      val dts = new PrintStream(dihedralTempFile)
      var dcount = 0
      val dihedralTypeCount = scala.collection.mutable.Map[DihedralType,Int]()
      dihedrals.foreach { dihedral =>
        dcount = dcount + 1
        dihedralTypeCount(dihedral.dihedralType) = 1 + dihedralTypeCount.getOrElse(dihedral.dihedralType, 0)
        val (dihedralTypeName, params) = dihedral.dihedralType match {
          case cos: CosDihedralType => ("cos", List(cos.a, cos.delta, cos.m))
          case _ => ("unknown", Nil)
        }

        if (debug) {
          dts.print("%s %7.2f " format ( dihedral.dihedralType, math.toDegrees(dihedral.angle)))
        }

        dts.print("%-4s %8d %8d %8d %8d".format(
          dihedralTypeName,
          config.indexWhere(_.real == dihedral.atom1.real) + 1,
          config.indexWhere(_.real == dihedral.atom2.real) + 1,
          config.indexWhere(_.real == dihedral.atom3.real) + 1,
          config.indexWhere(_.real == dihedral.atom4.real) + 1))

        params.foreach { param => dts.print(" %12.6f".format(param)) }
        dts.println
      }
      dts.close()

      out.println("DIHEDRALS %d".format(dcount))

      if (debug) {
          dihedralTypeCount.foreach {
              case (dihedralType,count) => 
                print("%s %7d\n" format ( dihedralType, count))
          }
      }

      Source.fromFile(dihedralTempFile).getLines().foreach {
        line => out.println(line)
      }
    }
  }
}
