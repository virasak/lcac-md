package lcac.md.dlpoly

import scala.io.Source
import lcac.md._
import java.io.{File, PrintStream}

/**
 * Application for export input file into PDB format
 */
object PdbExporter {

  val APPNAME = System.getProperty("command.name")

  val NEWLINE = System.getProperty("line.separator")

  def main(args: Array[String]) {
    val inputFileName = args(0)
    val inputFile = new File(inputFileName).getCanonicalFile
    val inputString = Source.fromFile(inputFile).getLines().mkString(NEWLINE)

    val parser = new SystemConfigParsers(inputFile.getParentFile)
    parser.parseAll(parser.system, inputString) match {
        case parser.Success(system,_) =>
            val pdbWriter = new PrintStream(inputFileName + ".pdb")
            exportToPdb(pdbWriter, system)

        case parser.NoSuccess(msg,_) =>
            println(msg)
    }

  }

  def exportToPdb(out: PrintStream, system: SystemConfig) = {
    out.println("REMARK LCAC-MD PDB Exporter")
    /*
    system.untCell match {
        case Some(cell) =>
            out.println(
                "CRYST1%9.3f%9.3f%9.3f%7.2f%7.2f%7.2f P1         1   " format (
                    cell.a, cell.b, cell.c,cell.alpha, cell.beta, cell.gamma
                )
            )
    }
    */
    //out.println("         1         2         3         4         5         6         7         8")
    //out.println("12345678901234567890123456789012345678901234567890123456789012345678901234567890")
    for (mol <- system.molecules) {
      for ((hetatm, index) <- mol.config.zipWithIndex) {
        out.println(
          "HETATM%5d %4s%c%3s%c%4s%c    %8.3f%8.3f%8.3f                      %-2s" format (
          index + 1,       // serial
          hetatm.atomType.name, // name
          ' ',         // altLoc
          "   ",       // resName
          ' ',         // chainID
          "    ",           // resSeq
          ' ',         // iCode
          hetatm.x,    // x
          hetatm.y,     // y
          hetatm.z,     // z
          hetatm.atomType.elementType.name // element
        ))
      }

      val config = mol.config
      for (bond <- mol.bonds) {
          out.println("CONECT%5d%5d" format (
            config.map(_.real).indexOf(bond.atom1.real) + 1, // first atom index
            config.map(_.real).indexOf(bond.atom2.real) + 1  // second atom index
          ))
      }
    }

    out.println("END")
  }
}
