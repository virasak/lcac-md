package lcac.md.dlpoly

import scala.io.Source
import lcac.md._
import java.io.{File, PrintStream}

object SuperCell {

  val APPNAME = "SuperCell"

  val NEWLINE = System.getProperty("line.separator")


  def main(args: Array[String]) {
    if (args.size == 4) {
      val a = args(0).toInt
      val b = args(1).toInt
      val c = args(2).toInt
      val inputFileName = args(3)
      val inputFile = new File(inputFileName).getCanonicalFile
      val inputString = Source.fromFile(inputFile).getLines().mkString(NEWLINE)
      val parser = new SystemConfigParsers(inputFile.getParentFile)
      parser.parseAll(parser.system, inputString) match {
          case parser.Success(system,_) =>
              val pdbWriter = new PrintStream("%s-%sx%sx%s.xyz" format (inputFileName, a, b, c))
              printSuperCell(pdbWriter, system, a, b, c)

          case parser.NoSuccess(msg,_) =>
            println(msg)
      }

    } else {
      println("Help me please")
    }
  }

  def printSuperCell(out: PrintStream, system: SystemConfig, a: Int, b: Int, c: Int) = {
      out.println(a * b * c * system.molecules.foldLeft(0) {(acc, mol) => acc + mol.config.size})
      out.println("just a comment")
      for {
          l <- 0 until a
          m <- 0 until b
          n <- 0 until c
      } {
          val dR = system.unitCell.get.latticePoint(l, m, n)
          println(dR)
          for (mol <- system.molecules; atom <- mol.config) {
              out.println("%s %f %f %f" format (atom.atomType.elementType.name, atom.x + dR.x, atom.y + dR.y, atom.z + dR.z))
          }
      }
  }

}
