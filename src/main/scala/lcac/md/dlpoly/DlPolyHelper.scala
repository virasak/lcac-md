package lcac.md.dlpoly

import scala.io.Source
import lcac.md._
import java.io.File

/**
 * Application class to export input files into DLPOLY file format
 */
object DlPolyHelper {

  val APPNAME = System.getProperty("command.name")

  val NEWLINE = System.getProperty("line.separator")

  def main(args: Array[String]) {
    val cmd = parse(args)
    if (cmd.help) {
      println(cmd.usage)
    } else {
      cmd.fileName match {
        case None => println(cmd.usage)
        case Some(inputFileName) =>
          val inputFile = new File(inputFileName).getCanonicalFile
          val inputString = Source.fromFile(inputFile).getLines().mkString(NEWLINE)

          val parser = new SystemConfigParsers(inputFile.getParentFile)
          parser.parseAll(parser.system, inputString) match {
              case parser.Success(system,_) =>
                  val writer = new DlPolyWriter(system, cmd.debug)

                  val configFile = new java.io.PrintStream(inputFileName + ".config")
                  val fieldFile = new java.io.PrintStream(inputFileName + ".field")
                  writer.writeConfig(configFile)
                  writer.writeField(fieldFile)
              case parser.NoSuccess(msg,_) => println(msg)
          }

      }
    }
  }

  /**
   * Cli option extractor
   */
  private def parse(args: Array[String]) = {
    import org.apache.commons.cli.{Options, GnuParser, HelpFormatter}
    import java.io.{PrintWriter,StringWriter}

    val options = new Options
    options.addOption("h", "help", false, "help")
    options.addOption("v", "verbose", false, "verbose mode")
    options.addOption("d", "debug", false, "print additional data into DLPOLY files for debugging")
    val cmd = (new GnuParser).parse(options, args)

    new {
      def usage = {
        val out = new StringWriter
        (new HelpFormatter).printUsage(new PrintWriter(out), 40, APPNAME, options)
        out.getBuffer
      }

      val help = cmd.hasOption("h")
      val verbose = cmd.hasOption("v")
      val debug = cmd.hasOption("d")
      val fileName = if (cmd.getArgs.size > 0) Some(cmd.getArgs.apply(0)) else None
    }
  }
}
