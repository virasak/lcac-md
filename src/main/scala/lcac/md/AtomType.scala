package lcac.md

case class AtomType(
  name: String,
  elementType: ElementType,
  numberOfBonds: Int,
  charge: Double,
  sites: List[Vector] = Nil) {

  lazy val atoms: List[Atom] = for (site <- sites) yield Atom(this, site)

  def numberOfAtoms: Int = sites.size
}
