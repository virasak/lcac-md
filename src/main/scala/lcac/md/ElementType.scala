package lcac.md

/**
 * Representation of chemistry element
 */
case class ElementType(name: String, atomicMass: Double)
