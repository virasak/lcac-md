package lcac.md

import scala.io.Source
import lcac.md.field.bonding._

class Molecule(
  val name: String,
  val atomTypes: List[AtomType],
  val actualBondTypes: List[BondType] = Nil,
  val virtualBondTypes: List[VirtualBondType] = Nil,
  val angleTypes: List[AngleType] = Nil,
  val dihedralTypes: List[DihedralType] = Nil,
  val unitCell: Option[UnitCell] = None) {

  /**
   * Number of atom sites
   */
  val numberOfAtoms = atomTypes.foldLeft(0) {
    (acc,atomType) => acc + atomType.numberOfAtoms
  }

  /**
   * Molecular configuration or list of atom sites
   */
  val config: List[Atom] =  atomTypes.flatMap(_.atoms)

  /**
   * All bonds with force field
   */
  val bonds: List[Bond] = {
    val sortedBonds = candidatedBonds.sortBy(_.distance)

    config.toSet[Atom].flatMap { atom =>
      sortedBonds.view.filter(_ contains atom).take(atom.numberOfBonds)
    }.toList
  }

  /**
   * Virual bonds that be used by improper dihedrals
   */
  val virtualBonds: List[Bond] = virtualBondTypes flatMap { bondType =>
    val allBonds = bondType.candidateBonds(unitCell).sortBy(_.distance)
    val atoms = allBonds.foldLeft(Set[Atom]()) { (acc,bond) => acc + bond.atom1 + bond.atom2 }

    atoms flatMap { atom =>
      allBonds.view.filter(_ contains atom).take(bondType.numberOfBonds)
    } toList
  }

  /**
   * Iterator for angles
   */
  def angles: Iterator[Angle] = angleTypes.iterator.flatMap(_.angles(bonds))

  /**
   * Iterator for dihedrals without improper dihedral
   */
  def dihedrals: Iterator[Dihedral] = dihedralTypes.iterator.flatMap(_.dihedrals(bonds))

  /**
   * Every pairs of atom that can bond
   */
  def candidatedBonds: List[Bond] = actualBondTypes.flatMap(_.candidateBonds(unitCell))

}
