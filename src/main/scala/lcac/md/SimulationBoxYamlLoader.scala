package lcac.md
/*
import java.io.{FileInputSream, File}
import java.util.{Map => JMap, Collection => JCollection}
import scala.collection.JavaConversions._


object SimulationBoxYamlLoader {
  type Dict = Map[String,_]

  def load(yamlFile: File): SimulationBox = {
    val yaml = new Yaml
    val map: Dict = yaml.load(new FileInputStream(yamlFile)).asInstanceOf[JMap[String,_]]

    val energyUnit = map("unit")
    val unitCell = loadUnitCell(map("unitCell"))
    val molecules = loadMolecules(map("molecules").asInstanceOf[JCollection])

    new SimulationSystem(energyUnit, unitCell, atomTypes)
  }

  def loadUnitCell(map: Dict): UnitCell = {
    map("type") match {
      case "cubic" =>
        val size = map("size").get.asInstanceOf[Double]
        new CubicUnitCell(size)
      case name => throw new RuntimeException("Not found unit cell name: " + name)
    }
  }

  def loadMolecules(iter: Iterable[Dict]): Iterable[Molecule] = iter.map(loadMolecule _)

  def loadMolecule(map: Map[Dict]): Molecule = {
    val name = map.string("type")
    val repeat = map.int("repeat", 1)
    val atomTypes = map.list("atoms").map(loadAtom _)
  }

  def loadAtom(map: Map[Dict]): (AtomType,Int) = {
  }

}
*/
