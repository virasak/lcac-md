package lcac.md.field.nonbonding

import lcac.md._

/**
 *
 */
trait FourBodyPotential {

  /**
   *
   */
  def aType: AtomType

  /**
   *
   */
  def bType: AtomType

  /**
   *
   */
  def cType: AtomType

  /**
   *
   */
  def dType: AtomType
}

/**
 *
 */
case class Harmonic4(aType: AtomType, bType: AtomType, cType: AtomType, dType: AtomType, k: Double, psi: Double) extends FourBodyPotential
