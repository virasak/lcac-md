package lcac.md.field.nonbonding

import lcac.md._

/**
 *
 */
trait ThreeBodyPotential {

  /**
   *
   */
  def aType: AtomType

  /**
   *
   */
  def bType: AtomType

  /**
   *
   */
  def cType: AtomType
}

/**
 *
 */
case class TruncatedHarmonic3(
  aType: AtomType,
  bType: AtomType,
  cType: AtomType,
  k: Double,
  zeta: Double,
  rho: Double,
  rho2: Double,
  rcut: Double) extends ThreeBodyPotential
