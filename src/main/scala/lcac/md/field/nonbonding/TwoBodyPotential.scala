package lcac.md.field.nonbonding

import lcac.md._

trait TwoBodyPotential {

  /**
   *
   */
  def aType: AtomType

  /**
   *
   */
  def bType: AtomType
}

/**
 *
 */
case class LernnardJones( aType: AtomType, bType: AtomType, epsilon: Double, zigma: Double) extends TwoBodyPotential
