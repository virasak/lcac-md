package lcac.md.field.bonding

import lcac.md.{Atom, ImageAtomSite, Vector}

/**
 * Representation of bond
 */
trait Bond {

  def bondType: BondType

  def atom1: Atom

  def atom2: Atom

  def distance = atom1.distance(atom2)

  def force = bondType(distance)

  def isImageBond: Boolean = atom1.isInstanceOf[ImageAtomSite] || atom2.isInstanceOf[ImageAtomSite]

  def direction = atom2.position - atom1.position

  def angle(that: Bond): Double = this.direction.angle(that.direction)

  def dihedralAngle(that: Bond): Double = {
    val axis = this.atom1.position - that.atom1.position
    val refplan = that.direction.cross(axis)
    val plan = this.direction.cross(axis)
    val ang = plan.angle(refplan)
    if (refplan.angle(this.direction) > math.Pi/2) ang else -ang
  }

  def contains(atom: Atom): Boolean = atom1.real == atom.real || atom2.real == atom.real
}

/**
 * Normal bond
 */
case class DirectBond (bondType: BondType, atom1: Atom, atom2: Atom) extends Bond {
  require(atom1 != atom2)
}

/**
 * Invert of normal bond
 */
case class InvertBond (invert: Bond) extends Bond {

  override def bondType = invert.bondType

  override def atom1 = invert.atom2

  override def atom2 = invert.atom1
}

object Bond {

  def apply(bondType: BondType, atom1: Atom, atom2: Atom): Bond = DirectBond(bondType, atom1, atom2)

  def invert(bondType: BondType, atom1: Atom, atom2: Atom): Bond = InvertBond(DirectBond(bondType, atom1, atom2))

  def invert(bond: Bond): Bond = if(bond.isInstanceOf[InvertBond]) bond.asInstanceOf[InvertBond].invert else InvertBond(bond)
}
