package lcac.md.field.bonding

import lcac.md._

import math.{pow, exp}

/**
 *
 */
trait BondType extends Function1[Double,Double] {

  def type1: AtomType

  def type2: AtomType

  def isAA: Boolean = type1 == type2


  def equiv(other: BondType): Boolean = {
    isMatch(other.type1, other.type2)
  }

  def isMatch(a: AtomType, b: AtomType): Boolean = {
    (type1 == a && type2 == b) || (type2 == a && type1 == b)
  }


  /**
   * List all posible bonds that represent bond type
   */
  def candidateBonds(unitCell: Option[UnitCell]): List[Bond] = {
    val atomAs = type1.atoms

    val bonds = if (isAA) {
      for {
        atomA <- atomAs.init
        atomB <- atomAs.dropWhile(_.real != atomA.real).tail
      } yield Bond(this, atomA, atomB)
    } else {
      val atomBs = type2.atoms
      for {
        atomA <- atomAs
        atomB <- atomBs
      } yield Bond(this, atomA, atomB)
    }

    unitCell match {
      case None=> bonds
      // form bond between unit cells if bond distance greater than half minimum width
      case Some(periodic) =>
        val halfMinWidth = periodic.minWidth / 2.0
        bonds map { bond =>
          if (bond.distance <= halfMinWidth) bond
          else {
            val images = periodic.imagesOf(bond.atom1.position, 1)
            val (position, distance) = images.foldLeft((bond.atom1.position, bond.distance)) { (rd, image) =>
                val  d = bond.atom2.position.distance(image)
                if (rd._2 <= d) rd else (image, d)
            }

            if (distance == bond.distance) bond
            else Bond(this, Atom.imageOf(bond.atom1, position), bond.atom2)
          }
        }
    }
  }

  override def toString = type1.name + "-" + type2.name
}

/**
 *
 */
case class VirtualBondType(
  type1: AtomType,
  type2: AtomType,
  numberOfBonds: Int = 1) extends BondType {

  def apply(r: Double): Double = 0.0 // No virtual bond force
}

/**
 *
 */
case class A12R6BondType(
  type1: AtomType,
  type2: AtomType,
  a: Double,
  b: Double) extends BondType {

  def apply(r: Double): Double = {
    val r6 = pow(r, 6)
    val r12 = r6*r6

    a/r12 - b/r6
  }
}

/**
 *
 */
case class HarmonicBondType(
  type1: AtomType,
  type2: AtomType,
  k: Double,
  r0: Double) extends BondType {

  private val K = k/2

  def apply(r: Double): Double = {
    val diff = r - r0

    K*diff*diff
  }
}

/**
 *
 */
case class MorseBondType(
  type1: AtomType,
  type2: AtomType,
  e0: Double,
  r0: Double,
  k: Double) extends BondType {

  def apply(r: Double): Double = {
    val expterm = 1 - exp(-k*(r - r0))
    val expterm2 = expterm*expterm

    e0*(expterm2 - 1)
  }
}
