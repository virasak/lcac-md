package lcac.md.field.bonding

/**
 *
 */
case class Dihedral(dihedralType: DihedralType, bond1: Bond, bond2: Bond) {

  require(bond1 != bond2)

  def atom1 = bond1.atom2

  def atom2 = bond1.atom1

  def atom3 = bond2.atom1

  def atom4 = bond2.atom2

  def angle = bond1.dihedralAngle(bond2)

  def force = dihedralType(angle)

}
