package lcac.md.field.bonding

import lcac.md._
import math.{cos}

/**
 * Representation of dihedral angle
 */
trait DihedralType extends Function1[Double,Double] { self =>

  def type1: AtomType

  def type2: AtomType

  def type3: AtomType

  def type4: AtomType

  def isAAA: Boolean = type1 == type2 && type2 == type3 && type3 == type4

  def isABA: Boolean = type1 == type4 && type2 == type3 && type1 != type2

  def equiv(other: DihedralType): Boolean = {
    (
      type1 == other.type1 &&
      type2 == other.type2 &&
      type3 == other.type3 &&
      type4 == other.type4
    ) ||
    (
      type1 == other.type4 &&
      type2 == other.type3 &&
      type3 == other.type2 &&
      type4 == other.type1
    )
  }

  /**
   * Dihedral extractor
   */
  private object Connected {

    def unapply(triple: (Bond,Bond,Bond)): Option[Dihedral] = Option {
      val (b1, b2, b3) = triple

      if (b1.atom2.real == b2.atom1.real) {
        if (b2.atom2.real == b3.atom1.real) {
          if (b1.atom1.real != b3.atom2.real) {
            // (1 2)(1 2)(1 2)
            Dihedral(self, Bond.invert(b1), b3)
          } else {
              null
          }
        } else if (b2.atom2.real == b3.atom2.real) {
          if (b1.atom1.real != b3.atom1.real) {
              // (1 2)(1 2)(2 1)
              Dihedral(self, Bond.invert(b1), Bond.invert(b3))
          } else {
              null
          }
        } else {
          null
        }
      } else if (b1.atom2.real == b2.atom2.real) {
        if (b2.atom1.real == b3.atom1.real) {
          if (b1.atom1.real != b3.atom2.real) {
              // (1 2)(2 1)(1 2)
              Dihedral(self, Bond.invert(b1), b3)
          } else {
              null
          }
        } else if (b2.atom1.real == b3.atom2.real) {
          if (b1.atom1.real != b3.atom1.real) {
              // (1 2)(2 1)(2 1)
              Dihedral(self, Bond.invert(b1), Bond.invert(b3))
          } else {
              null
          }
        } else {
          null
        }
      } else if (b1.atom1.real == b2.atom1.real) {
        if(b2.atom2.real == b3.atom1.real) {
          if (b1.atom2.real != b3.atom2.real) {
              // (2 1)(1 2)(1 2)
              Dihedral(self, b1, b3)
          } else {
              null
          }
        } else if (b2.atom2.real == b3.atom2.real) {
          if (b1.atom2.real != b3.atom1.real) {
              // (2 1)(1 2)(2 1)
              Dihedral(self, b1, Bond.invert(b3))
          } else {
              null
          }
        } else {
          null
        }
      } else if (b1.atom1.real == b2.atom2.real) {
        if (b2.atom1.real == b3.atom1.real) {
          if (b1.atom2.real != b3.atom2.real) {
              // (2 1)(2 1)(1 2)
              Dihedral(self, b1, b3)
          } else {
              null
          }
        } else if (b2.atom1.real == b3.atom2.real) {
          if (b1.atom2.real != b3.atom1.real) {
              // (2 1)(2 1)(2 1)
              Dihedral(self, b1, Bond.invert(b3))
          } else {
              null
          }
        } else {
          null
        }
      } else {
        null
      }
    }
  }

  /**
   *
   */
  def dihedrals(bonds: List[Bond]): Iterator[Dihedral] = {
    val bonds1 = bonds.filter(_.bondType.isMatch(type1, type2))

    val candidateDihedrals: Iterator[(Bond,Bond,Bond)] = if (isAAA) {
      for {
        bond1 <- bonds1.iterator
        bond3 <- bonds1.iterator.dropWhile(_ != bond1).drop(1)
        bond2 <- bonds1.iterator if bond2 != bond1 && bond2 != bond3
      } yield (bond1,bond2,bond3)
    } else if (isABA) {
      val bonds2 = bonds.filter(_.bondType.isMatch(type2, type2))
      for {
        bond1 <- bonds1.iterator
        bond3 <- bonds1.iterator.dropWhile(_ != bond1).drop(1)
        bond2 <- bonds2.iterator
      } yield (bond1,bond2,bond3)

    } else {
      val bonds2 = bonds.filter(_.bondType.isMatch(type2, type3))
      val bonds3 = bonds.filter(_.bondType.isMatch(type3, type4))
      for {
        bond1 <- bonds1.iterator
        bond2 <- bonds2.iterator if bond1 != bond2
        bond3 <- bonds3.iterator if bond2 != bond3 && bond1 != bond3
      } yield (bond1,bond2,bond3)
    }
    for (Connected(dih) <- candidateDihedrals) yield dih
  }

  override def toString = type1.name + "-" + type2.name + "-" + type3.name + "-" + type4.name

}

/**
 * Harmonic force field
 */
case class HarmonicType(
  type1: AtomType,
  type2: AtomType,
  type3: AtomType,
  type4: AtomType,
  k: Double, zeta: Double) extends DihedralType {

  private val K = k/2

  def apply(phi: Double): Double = {
    val diff = phi - zeta

    K*diff*diff
  }
}
/**
 * Cosine force field
 */
case class CosDihedralType(
  type1: AtomType,
  type2: AtomType,
  type3: AtomType,
  type4: AtomType,
  a: Double,
  delta: Double,
  m: Double) extends DihedralType {

  def apply(phi: Double): Double = {
    a*(1 + cos(m*phi - delta))
  }
}
