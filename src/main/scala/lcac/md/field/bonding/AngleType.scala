package lcac.md.field.bonding

import lcac.md._
import math._

/**
 * Representation of bond angle force field
 */
trait AngleType extends Function1[Double,Double] { self =>

  def type1: AtomType

  def type2: AtomType

  def type3: AtomType

  def isABA: Boolean = type1 == type3

  def equiv(other: AngleType): Boolean = {
    (type2 == other.type2) &&
    (
      (type1 == other.type1 && type3 == other.type3) ||
      (type1 == other.type3 && type3 == other.type1)
    )
  }

  def angles(bonds: List[Bond]): Iterator[Angle] = {
    for (Connected(angle) <- bondPairs(bonds)) yield angle
  }

  private object Connected {
    def unapply(pair: (Bond,Bond)): Option[Angle] = {
      val (b1, b2) = pair
      Option(
        if (b1.atom1.real == b2.atom1.real && b1.atom1.atomType == type2) Angle(self, b1, b2)
        else if (b1.atom1.real == b2.atom2.real && b1.atom1.atomType == type2) Angle(self, b1, Bond.invert(b2))
        else if (b1.atom2.real == b2.atom1.real && b1.atom2.atomType == type2) Angle(self, Bond.invert(b1), b2)
        else if (b1.atom2.real == b2.atom2.real && b1.atom2.atomType == type2) Angle(self, Bond.invert(b1), Bond.invert(b2))
        else null
      )
    }
  }

  private def bondPairs(bonds: List[Bond]): Iterator[(Bond,Bond)] = {
    val bonds1 = bonds.filter(_.bondType.isMatch(type1, type2))

    if (isABA) {
      for {
        bond1 <- bonds1.init.iterator
        bond2 <- bonds1.dropWhile(_ != bond1).tail.iterator
      } yield (bond1, bond2)
    } else {
      val bonds2 = bonds.filter(_.bondType.isMatch(type2, type3))
      for {
        bond1 <- bonds1.iterator
        bond2 <- bonds2.iterator
      } yield (bond1, bond2)
    }
  }

  override def toString = type1.name + "-" + type2.name + "-" + type3.name
}

/**
 * Harmonic valence angle force field
 */
case class HarmonicAngleType(
  type1: AtomType,
  type2: AtomType,
  type3: AtomType,
  k: Double,
  zeta: Double) extends AngleType {

    private val K = k/2

    def apply(phi: Double): Double = {
      K*pow(phi - zeta, 2)
    }
}

/**
 * Harmonic cosine valence angle force field
 */
case class HCosAngleType(
  type1: AtomType,
  type2: AtomType,
  type3: AtomType,
  k: Double,
  zeta: Double) extends AngleType {

    private val K = k/2
    private val cosZeta = cos(zeta)

    def apply(phi: Double): Double = {
      val diff = cos(phi) - cosZeta

      K*diff*diff
    }
}

/**
 * Quartic valence angle force field 
 */
case class QuarticAngleType(
  type1: AtomType,
  type2: AtomType,
  type3: AtomType,
  k: Double,
  zeta: Double,
  kp: Double,
  kpp: Double) extends AngleType {

    private val K2 = k/2
    private val K3 = kp/2
    private val K4 = kpp/2

    def apply(phi: Double): Double = {
      val diff1 = phi - zeta
      val diff2 = diff1*diff1
      val diff3 = diff2*diff1
      val diff4 = diff3*diff1

      K2*diff2 + K3*diff3 + K4*diff4
    }
}
