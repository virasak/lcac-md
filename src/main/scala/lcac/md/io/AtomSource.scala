package lcac.md.io

import scala.io.Source

import lcac.md.{AtomType, Vector}

/**
 * Utility object for loading atoms from resource
 */
object AtomSource {


  /**
   * Load atoms from PDB file format
   */
  def fromPDB(src: Source, atomTypes: List[AtomType]): List[AtomType] = {

    require(atomTypes.size == atomTypes.map(_.name).distinct.size)

    object AtomRecord {
      // PDB atom line spec.
      val AtomNameRange = (12, 15)
      val XRange = (30, 37)
      val YRange = (38, 45)
      val ZRange = (46, 53)

      /**
       * wrap substring to have index like PDB atom record format
       * (inclusive for both ends of index)
       */
      def record(line: String)(range: (Int,Int)): String = line.substring(range._1, range._2 + 1).trim

      def unapply(line: String): Option[(String,Vector)] = {
        if (line.startsWith("ATOM  ")) {
          val atomRecord = record(line) _
          val name = atomRecord(AtomNameRange)
          val x = atomRecord(XRange).toDouble
          val y = atomRecord(YRange).toDouble
          val z = atomRecord(ZRange).toDouble
          Some((name, Vector(x, y, z)))
        } else {
          None
        }
      }
    }

    val axyz = (
      for {
        AtomRecord(name, xyz) <- src.getLines()
        atomType              <- atomTypes.iterator if atomType.name == name
      } yield (atomType, xyz)
    ).toList.groupBy(_._1)

    for (atomType <- atomTypes) yield atomType.copy(sites = axyz(atomType) map (_._2))
  }

  /**
   * load atoms from XYZ file format
   */
  def fromXYZ(src: Source, skip: Int, atomTypes: List[(AtomType,Int)], moleculeCount: Int): List[AtomType] = {

    object AtomRecord {
      def unapply(line: String): Option[(String,Vector)] = line.trim.split("""\s+""").toSeq match {
        case Seq(name, x, y, z) => Some((name, Vector(x.toDouble, y.toDouble, z.toDouble)))
        case _ => None
      }
    }

    // Read atoms from xyz file
    val moleculeSize = atomTypes map (_._2) reduceLeft (_+_)
    val totalSize = moleculeSize * moleculeCount
    val HeaderLineSize = 2
    val atoms = for {
      AtomRecord(name, xyz)<- src.getLines().drop(HeaderLineSize).drop(skip).take(totalSize)
    } yield (name, xyz)

    val collector = scala.collection.mutable.Map[AtomType,List[Vector]]()
    atoms.grouped(moleculeSize).foreach { block =>
      atomTypes.foldLeft(block) { (remainBlock, pair) =>
        val (atomType,numberOfAtoms) = pair
        val (headBlock, tailBlock) = remainBlock.splitAt(numberOfAtoms)
        val elem = atomType.elementType.name
        val vectors = for ((elem, vector) <- headBlock) yield vector
        require(headBlock.size == vectors.size)
        collector(atomType) = vectors.toList ++ collector.getOrElse(atomType, List[Vector]())

        // return reduced block
        tailBlock
      }
    }

    for ((atomType,_) <- atomTypes) yield atomType.copy(sites = collector(atomType))
  }
}
