package lcac.md

/**
 * Atom site
 */
sealed trait Atom {

  /**
   * the original site on the unit cell
   */
  def real: Atom

  /**
   * Site property
   */
  def atomType: AtomType

  /**
   * Site position
   */
  def position: Vector

  def x = position.x

  def y = position.y

  def z = position.z

  def charge = atomType.charge

  def distance(that: Atom) = position.distance(that.position)

  def numberOfBonds = atomType.numberOfBonds
}

case class AtomSite (atomType: AtomType, position: Vector) extends Atom {
    def real = this
}

case class ImageAtomSite (real: Atom, position: Vector) extends Atom {
  def atomType = real.atomType
}

object Atom {

  def apply(atomType: AtomType, position: Vector): Atom = {
    AtomSite(atomType, position)
  }

  def imageOf(atom: Atom, position: Vector): Atom = {
    new ImageAtomSite(atom, position)
  }
}
